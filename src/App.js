import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import HeaderComponent from './components/HeaderComponent';
import FooterComponent from './components/FooterComponent';
import IntroducComponent from './components/Content/IntroduceComponent';
import SizeComponent from './components/Content/SizeComponent';
import DrinkComponent from './components/Content/DrinkComponent';
import FormComponent from './components/Content/FormComponent';

function App() {
  return (
    <div>
     <HeaderComponent/>
     <IntroducComponent/>
     <SizeComponent/>
     <DrinkComponent/>
     <FormComponent/>
     <FooterComponent/>

    </div>
  );
}

export default App;
