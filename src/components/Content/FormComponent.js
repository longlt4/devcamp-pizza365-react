import { Component } from "react";

class FormComponent extends Component {
    render(){
        return (
            <div className="container">
            {/* <!-- Title Contact Us --> */}
            <div className="col-sm-12 text-center p-4 mt-4">
                <h2><b className="p-2 border-bottom text-warning">Thông tin đơn hàng</b></h2>
            </div>
            {/* <!-- Content Contact Us --> */}
            <div className="col-sm-12 p-2 jumbotron">
                <div className="row">
                    <div className="col-sm-12">
                        <div className="form-group">
                            <label htmlFor="fullname">Họ và tên</label>
                            <input type="text" className="form-control" id="inp-fullname" placeholder="Họ và tên" defaultValue="Lê Thanh Long" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="email">Email</label>
                            <input type="text" className="form-control" id="inp-email" placeholder="Email" defaultValue="lethanhlong137@gmail.com" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="dien-thoai">Điện thoại</label>
                            <input type="text" className="form-control" id="inp-dien-thoai" placeholder="Điện thoại" defaultValue="0909999999" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="dia-chi">Địa chỉ</label>
                            <input type="text" className="form-control" id="inp-dia-chi" placeholder="Địa chỉ" defaultValue="Nguyễn Thị Minh khai" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="message">Lời nhắn</label>
                            <input type="text" className="form-control" id="inp-message" placeholder="Lời nhắn" defaultValue="Khỏi giao" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="voucher">Mã giảm giá (Voucher ID)</label>
                            <input type="text" className="form-control" id="inp-voucher" placeholder="Mã giảm giá" />
                        </div>
                        <br></br>
                        <button type="button" className="btn btn-warning w-100">
                            Kiểm tra đơn
                        </button>
                    </div>
                </div>
            </div>
        </div>
        )
    }
}

export default FormComponent;