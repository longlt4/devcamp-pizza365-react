import { Component } from "react";
const Images = {
    carousel1: require('../../assetes/images/content/1.jpg'),
    carousel2: require('../../assetes/images/content/2.jpg'),
    carousel3: require('../../assetes/images/content/3.jpg'),
    carousel4: require('../../assetes/images/content/4.jpg'),
};

class IntroducComponent extends Component {
    render(){
        return (
            <div className="row">
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-12 orange">
                            <h1><b>PIZZA 365</b></h1>
                            <p style={{ fontSize: "36px", fontWeight: "bolder", fontStyle: "italic" }}>Truly italian!</p>
                        </div>
                        <div className="col-sm-12">
                            <div id="carouselExampleIndicators" className="carousel slide" data-ride="carousel">
                                <ol className="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                                </ol>
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <img className="d-block w-100" src={Images.carousel1} alt="First slide" />
                                    </div>
                                    <div className="carousel-item">
                                        <img className="d-block w-100" src={Images.carousel2} alt="Second slide" />
                                    </div>
                                    <div className="carousel-item">
                                        <img className="d-block w-100" src={Images.carousel3} alt="Third slide" />
                                    </div>
                                    <div className="carousel-item">
                                        <img className="d-block w-100" src={Images.carousel4} alt="Four slide" />
                                    </div>
                                </div>
                                <a className="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                                    data-slide="prev">
                                    <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Previous</span>
                                </a>
                                <a className="carousel-control-next" href="#carouselExampleIndicators" role="button"
                                    data-slide="next">
                                    <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span className="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
        
           {/* Chose Pizza */}
            <div className="col-sm-12 text-center p-4 orange mt-4">
                <h2><b className="p-2 border-bottom orange">Tại sao lại Pizza 365</b></h2>
            </div>
            <div className="col-sm-12">
                <div className="row">
                    <div className="col-sm-3 p-4 bg-lightgoldenrodyellow text-dark border-orange">
                        <h3 className="p-2">Đa dạng</h3>
                        <h6 className="p-2">Số lượng pizza đa dạng. Có đầy đủ các loại pizza đang hot nhất hiện nay.
                        </h6>
                    </div>
                    <div className="col-sm-3 p-4 bg-yellow text-dark border-orange">
                        <h3 className="p-2">Chất lượng</h3>
                        <h6 className="p-2">Nguyên liệu sạch 100%, rõ nguồn gốc, qui trình chế biến đảm bảo vệ
                            sinh an toàn thực phẩm.</h6>
                    </div>
                    <div className="col-sm-3 p-4 bg-lightsalmon text-dark border-orange">
                        <h3 className="p-2">Hương vị</h3>
                        <h6 className="p-2">Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza
                            365.</h6>
                    </div>
                    <div className="col-sm-3 p-4 bg-orange text-dark border-orange">
                        <h3 className="p-2">Dịch vụ</h3>
                        <h6 className="p-2">Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh
                            chất lượng, tân tiến. </h6>
                    </div>
                </div>
            </div>

            </div>
        )
    }
}

export default IntroducComponent;