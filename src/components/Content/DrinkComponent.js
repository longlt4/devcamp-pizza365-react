import { Component } from "react";

class DrinkComponent extends Component {
    render() {
        return (
            <div>
                 {/* chon do uong */ }
                <div className="col-sm-12 text-center p-4 mt-4">
                    <div className="p-2 border-bottom">
                        <label><b className="p-2 border-bottom text-warning">Hãy chọn nước uống bạn yêu thích</b></label>
                    </div>
                    <div className="form-group">
                        <select placeholder="Chọn loại nước uống..." id="select-drink" className="form-control"></select>
                    </div>
                </div>
            </div>
        )
    }
}

export default DrinkComponent;