import { Component } from "react";
const Images = {
    hawai: require('../../assetes/images/content/hawaiian.jpg'),
    bacon: require('../../assetes/images/content/bacon.jpg'),
    seafood: require('../../assetes/images/content/seafood.jpg'),
};
class SizeComponent extends Component {
    render() {
        return (
            <div className="container">
                {/* Size Pizza */}
                <div className="row">
                    <div className="col-sm-12 text-center orange p-4 mt-4">
                        <h2><b className="p-1 border-bottom">Chọn size pizza</b></h2>
                        <p><span className="p-2">Hãy combo pizza phù hợp với nhu cầu của bạn.</span></p>
                    </div>
                    <div className="col-sm-12">
                        <div className="row combo">
                        </div>
                    </div>
                </div>
                <br></br>
                {/* Size  */}
                <div className="col-sm-12">
                    <div className="row">
                        <div className="col-sm-4">
                            <div className="card">
                                <div className="card-header bg-warning text-white text-center">
                                    <h4>S (Small)</h4>
                                </div>
                                <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item"><b>20cm</b> Đường kính</li>
                                        <li className="list-group-item"><b>1</b> Sườn nướng</li>
                                        <li className="list-group-item"><b>200g</b> Salad</li>
                                        <li className="list-group-item"><b>2</b> Nước ngọt</li>
                                        <li className="list-group-item">
                                            <h3><b>VNĐ 150.000</b></h3>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer text-center">
                                    <button id="sizeS" className="btn btn-warning">
                                        Chọn
                                    </button>
                                </div>
                            </div>
                        </div>

                        {/* Size M */}
                        <div className="col-sm-4">
                            <div className="card">
                                <div className="card-header bg-warning text-white text-center">
                                    <h4>M (Medium)</h4>
                                </div>
                                <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item"><b>25cm</b> Đường kính</li>
                                        <li className="list-group-item"><b>4</b> Sườn nướng</li>
                                        <li className="list-group-item"><b>300g</b> Salad</li>
                                        <li className="list-group-item"><b>3</b> Nước ngọt</li>
                                        <li className="list-group-item">
                                            <h3><b>VNĐ 200.000</b></h3>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer text-center">
                                    <button id="sizeM" className="btn btn-warning">
                                        Chọn
                                    </button>
                                </div>
                            </div>
                        </div>

                        {/* Size L */}
                        <div className="col-sm-4">
                            <div className="card">
                                <div className="card-header bg-warning text-white text-center">
                                    <h4>L (Large)</h4>
                                </div>
                                <div className="card-body text-center">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item"><b>30cm</b> Đường kính</li>
                                        <li className="list-group-item"><b>8</b> Sườn nướng</li>
                                        <li className="list-group-item"><b>500g</b> Salad</li>
                                        <li className="list-group-item"><b>4</b> Nước ngọt</li>
                                        <li className="list-group-item">
                                            <h3><b>VNĐ 250.000</b></h3>
                                        </li>
                                    </ul>
                                </div>
                                <div className="card-footer text-center">
                                    <button id="sizeL" className="btn btn-warning">
                                        Chọn
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                             {/* Chon loai PZ */}
            <div>
                <div className="col-sm-12 text-center orange p-4 mt-4">
                    <h2><b className="p-1 border-bottom">Chọn loại pizza</b></h2>
                </div>
            </div>
            <div className="col-sm-12">
                <div className="row">
                    <div className="col-sm-4">
                        <div className="card w-100" style={{ width: "18rem" }}>
                            <img src= { Images.hawai} alt="pizzahawai"className="card-img-top" style={{ height: "150px" }} />
                            <div className="card-body">
                                <h3>Hawai</h3>
                                <p>Thành phần</p>
                                <p>
                                    Jam bông, dứa, sốt cà chua, pho mai, xúc xích Salami,
                                    hành tây
                                </p>
                                <p>
                                    <button id="type-hawai" className="btn btn-warning w-100">
                                        Chọn
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="card w-100" style={{ width: "18rem" }}>
                            <img src={Images.seafood}  alt="pizzaSeafood"className="card-img-top" style={{ height: "150px" }} />
                            <div className="card-body" >
                                <h3>Hải Sản</h3>
                                <p>Thành phần</p>
                                <p>
                                    Thanh cua, tôm, ớt xanh, hành tây, cà chua, sốt cà chua,
                                    pho mai.
                                </p>
                                <p>
                                    <button id="type-hai-san" className="btn btn-warning w-100">
                                        Chọn
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-sm-4">
                        <div className="card w-100" style={{ width: "18rem" }}>
                            <img src={Images.bacon} alt="pizzaBacon" className="card-img-top " style={{ height: "150px" }} />
                            <div className="card-body">
                                <h3>Thịt hun khói</h3>
                                <p>Thành phần</p>
                                <p>
                                    Thịt bò hun khói, hành tây, nấm, sốt cà chua, phô mai, xúc xích.
                                </p>
                                <p>
                                    <button id="type-thit-hun-khoi" className="btn btn-warning w-100">
                                        Chọn
                                    </button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        )
    }
}
export default SizeComponent;