
import { Component } from "react";

class HeaderComponent extends Component {
    render(){
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-orange">
            <div className="container-fluid">
              <div className="row w-100">
                <div className="col-12">
                  <button className="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                  </button>
    
                  <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav nav-fill w-100">
                      <li className="nav-item">
                        <a className="nav-link" href="#"><b>Trang chủ</b></a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="#combo"><b>Combo</b></a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="#pizza"><b>Loại Pizza</b></a>
                      </li>
                      <li className="nav-item">
                        <a className="nav-link" href="#order"><b>Gửi đơn hàng</b></a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        )
    }
}

export default HeaderComponent;